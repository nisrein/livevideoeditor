package com.ca.video.editor;

import java.io.File;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.objdetect.CascadeClassifier;

import common.ImageUtils;
import common.Log;

public class Detector {
	private final static String TAG = Detector.class.getSimpleName();
	private CascadeClassifier cascadeClassifier;
	private String cascadeClassifierFilePath;
	private boolean loadSuccess = false;
	private double scaleDetectedAreaWidth = 0.2;
	private double scaleDetectedAreaHeight = 0.4;

	public Detector(String cascadeClassifierFilePath) {
		this.cascadeClassifierFilePath = cascadeClassifierFilePath;
		init();
		initCascadeClassifier(this.cascadeClassifierFilePath);
	}

	private void init() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}

	private void initCascadeClassifier(String cascadeClassifierFilePath) {
		Log.v(TAG, "initCascadeClassifier: " + cascadeClassifierFilePath);
		File file = new File(cascadeClassifierFilePath);
		boolean fileExists = file.exists();
		if (fileExists) {
			cascadeClassifier = new CascadeClassifier(cascadeClassifierFilePath);
			loadSuccess = true;
		}
	}

	public Rect[] smartDetect(Mat image) {
		Rect[] rects = detect(image);
		if (rects.length == 0) {
			for (int i = 1; i <= 30; i += 5) {
				Mat temp = ImageUtils.rotate(image, i);
				Rect[] tempRects = detect(temp);
				if (tempRects.length>=1) {
					return tempRects;
				}
			}
			
			for (int i = 1; i > 30; i -= 5) {
				Mat temp = ImageUtils.rotate(image, i);
				Rect[] tempRects = detect(temp);
				if (tempRects.length>=1) {
					return tempRects;
				}
			}
		}
		return rects;
	}
	
	public Rect[] detect(Mat image) {
		MatOfRect detections = new MatOfRect();
		if (loadSuccess) {
			cascadeClassifier.detectMultiScale(image, detections);
			Rect[] rects = detections.toArray();
			Log.v(TAG, String.format("Detected %s object: ", rects.length));
			rects = scale(rects);
			return rects;
		} else {
			Log.e(TAG, "detect: CascadeClassifier was not loaded successfully");
		}
		return detections.toArray();
	}
	
	private Rect[] scale(Rect[] rects) {
		for (Rect rect : rects) {
			rect.x = (int) (rect.x - rect.x * scaleDetectedAreaWidth/2);
			rect.x = Math.max(rect.x, 0);
			rect.width = (int) (rect.width + rect.width * scaleDetectedAreaWidth/2);
			
			rect.y = (int) (rect.y - rect.y * scaleDetectedAreaHeight/2);
			rect.y = Math.max(rect.y, 0);
			rect.height = (int) (rect.height + rect.height * scaleDetectedAreaHeight/2);
		}
		return rects;
	}
}
