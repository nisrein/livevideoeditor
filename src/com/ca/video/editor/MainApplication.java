package com.ca.video.editor;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class MainApplication {
	private static MainApplicationFrame frame;
	public static void main(String[] args) {
		initVideoFrame();
		startVideoFramePreview();
	}
	
	private static void initVideoFrame() {
		JFrame.setDefaultLookAndFeelDecorated(true);
		frame = new MainApplicationFrame();
		frame.setTitle("Live Video Editor");
	}
	
	private static void startVideoFramePreview() {
		EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                	//frame.pack();
                	//frame.setLocationRelativeTo(null);
            		frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
	}
}
