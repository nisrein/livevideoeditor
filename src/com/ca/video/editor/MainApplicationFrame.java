package com.ca.video.editor;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import org.opencv.core.Mat;
import common.ImagePicker;
import common.ImageUtils;
import common.Log;

public class MainApplicationFrame extends JFrame implements ImagePicker.OnImagePickerListener {
	private static final long serialVersionUID = 4612663191401704400L;
	public static final String TAG = MainApplicationFrame.class.getSimpleName();

	private MainVideoPanel mainVideoPanel;
	private BorderLayout borderLayout;
	private FlowLayout controlsLayout;
	private GridLayout featuresLayout;
	private ImagePicker imagePicker;

	/**
	 * Create the frame.
	 */
	public MainApplicationFrame() {
		init();
	}

	private void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 654, 528);
		setResizable(false);
		borderLayout = new BorderLayout(5, 5);
		getContentPane().setLayout(borderLayout);
		
		getContentPane().setComponentOrientation(
                java.awt.ComponentOrientation.LEFT_TO_RIGHT);
		
		this.addWindowListener(MyWindowListener);
		
		imagePicker = new ImagePicker();
		imagePicker.setOnImagePickerListener(MainApplicationFrame.this);

		mainVideoPanel = new MainVideoPanel();
		controlsLayout = new FlowLayout();
		
		JPanel southPanel = new JPanel();
		southPanel.setLayout(controlsLayout);
		
		final JToggleButton startVideoToggle = new JToggleButton("Start Preview");
		final JToggleButton smartDetectorToggle = new JToggleButton("Enbale Smart Detector");
		final JCheckBox drawBorderChkBox = new JCheckBox("Draw Borders");
		drawBorderChkBox.setSelected(true);
		
		startVideoToggle.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					startVideoToggle.setText("Stop Preview");
					mainVideoPanel.startVideoPreview();
				} else {
					startVideoToggle.setText("Start Preview");
					mainVideoPanel.stopVideoPreview();
				}
			}
		});
		
		smartDetectorToggle.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					smartDetectorToggle.setText("Disable Smart Detector");
					mainVideoPanel.setSmartDetector(true);
				} else {
					smartDetectorToggle.setText("Enbale Smart Detector");
					mainVideoPanel.setSmartDetector(false);
				}
				
			}
		});
		drawBorderChkBox.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					mainVideoPanel.setDrawDetectionBorder(true);
				} else {
					mainVideoPanel.setDrawDetectionBorder(false);
				}
			}
		});
		
		
		southPanel.add(startVideoToggle);
		southPanel.add(smartDetectorToggle);
		southPanel.add(drawBorderChkBox);
		
		southPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		
		JPanel leftPanel = initLeftPanel();
		
		if (mainVideoPanel != null) {
			getContentPane().add(mainVideoPanel, BorderLayout.CENTER);
		}
		
		getContentPane().add(southPanel, BorderLayout.PAGE_END);
		getContentPane().add(leftPanel, BorderLayout.WEST);
		if (mainVideoPanel != null) {
			mainVideoPanel.setVisible(true);
		}
		
		startVideoToggle.doClick();
	}

	private ButtonGroup buttonGroup;
	private JToggleButton normalToggle;
	private JToggleButton blurToggle;
	private JToggleButton pixilateToggle;
	private JToggleButton cannyToggle;
	private JToggleButton sobelToggle;
	private JToggleButton imageToggle;
	private JToggleButton erosionToggle;
	private JToggleButton swapToggle;
	private JToggleButton flipDownToggle;
	private JToggleButton flipRightToggle;
	private JToggleButton brightToggle;
	
	private JPanel initLeftPanel() {
		featuresLayout = new GridLayout(11, 0, 5, 5);
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(featuresLayout);
		
		buttonGroup = new ButtonGroup();
		
		normalToggle = new JToggleButton("Normal");
		normalToggle.setPreferredSize(new Dimension(150, 200));
		
		blurToggle = new JToggleButton("Blur");
		blurToggle.setPreferredSize(new Dimension(100, 200));
		
		pixilateToggle = new JToggleButton("Pixilate");
		pixilateToggle.setPreferredSize(new Dimension(100, 200));
		
		cannyToggle = new JToggleButton("Face Lines");
		pixilateToggle.setPreferredSize(new Dimension(100, 200));
		
		sobelToggle = new JToggleButton("Deep Dark");
		sobelToggle.setPreferredSize(new Dimension(100, 200));
		
		imageToggle = new JToggleButton("Change Image");
		imageToggle.setPreferredSize(new Dimension(100, 200));
		
		erosionToggle = new JToggleButton("Erosion");
		erosionToggle.setPreferredSize(new Dimension(100, 200));
		
		swapToggle = new JToggleButton("Swap");
		swapToggle.setPreferredSize(new Dimension(100, 200));
		
		flipDownToggle = new JToggleButton("Flip Down");
		flipDownToggle.setPreferredSize(new Dimension(100, 200));
		
		flipRightToggle = new JToggleButton("Flip Right");
		flipRightToggle.setPreferredSize(new Dimension(100, 200));
		
		brightToggle = new JToggleButton("Bright Face");
		brightToggle.setPreferredSize(new Dimension(100, 200));
		
		normalToggle.setSelected(true);
		buttonGroup.add(normalToggle);
		buttonGroup.add(blurToggle);
		buttonGroup.add(pixilateToggle);
		buttonGroup.add(cannyToggle);
		buttonGroup.add(sobelToggle);
		buttonGroup.add(imageToggle);
		buttonGroup.add(erosionToggle);
		buttonGroup.add(swapToggle);
		buttonGroup.add(flipDownToggle);
		buttonGroup.add(flipRightToggle);
		buttonGroup.add(brightToggle);
		 
		normalToggle.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					mainVideoPanel.setCurrentFeature(SupportedFeature.IMAGE_NORMAL);
				}
			}
		});
		
		blurToggle.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					mainVideoPanel.setCurrentFeature(SupportedFeature.IMAGE_BLUR);
				}
			}
		});
		
		pixilateToggle.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					mainVideoPanel.setCurrentFeature(SupportedFeature.IMAGE_PIXILATE);
				}
			}
		});
		
		cannyToggle.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					mainVideoPanel.setCurrentFeature(SupportedFeature.IMAGE_CANNY);
				}
			}
		});
		
		sobelToggle.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					mainVideoPanel.setCurrentFeature(SupportedFeature.IMAGE_SOBEL);
				}
			}
		});
		
		imageToggle.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					imagePicker.show(MainApplicationFrame.this);
				}
			}
		});
		
		erosionToggle.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					mainVideoPanel.setCurrentFeature(SupportedFeature.IMAGE_EROSION);
				}
			}
		});

		swapToggle.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					mainVideoPanel.setCurrentFeature(SupportedFeature.IMAGE_SWAP);
				}
			}
		});
		

		flipDownToggle.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					mainVideoPanel.setCurrentFeature(SupportedFeature.IMAGE_FLIP_DOWN);
				}
			}
		});
		
		flipRightToggle.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					mainVideoPanel.setCurrentFeature(SupportedFeature.IMAGE_FLIP_RIGHT);
				}
			}
		});
	
		brightToggle.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					mainVideoPanel.setCurrentFeature(SupportedFeature.IMAGE_BRIGHT);
				}
			}
		});
		
		leftPanel.add(normalToggle);
		leftPanel.add(blurToggle);
		leftPanel.add(pixilateToggle);
		leftPanel.add(cannyToggle);
		leftPanel.add(sobelToggle);
		leftPanel.add(imageToggle);
		leftPanel.add(erosionToggle);
		leftPanel.add(swapToggle);
		leftPanel.add(flipDownToggle);
		leftPanel.add(flipRightToggle);
		leftPanel.add(brightToggle);
		
		return leftPanel;
	}
	
	public void paint(Graphics g) {
		
	}
	
	private WindowListener MyWindowListener = new WindowListener() {

		@Override
		public void windowOpened(WindowEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void windowIconified(WindowEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void windowDeiconified(WindowEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void windowClosing(WindowEvent e) {
			if (mainVideoPanel != null) {
				mainVideoPanel.windowClosing(e);
			}
		}

		@Override
		public void windowClosed(WindowEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void windowActivated(WindowEvent e) {
			// TODO Auto-generated method stub

		}
	};

	@Override
	public void onImagePicked(String filePath) {
		Log.v(TAG, "onImagePicked: File Path: " + filePath);
		File file = new File(filePath);
		if(file.exists()) {
			Mat mat = ImageUtils.readImage(file.getAbsolutePath());
			mainVideoPanel.setSelectedImage(mat);
			mainVideoPanel.setCurrentFeature(SupportedFeature.IMAGE_PICKER);
		} else {
			normalToggle.doClick();
		}
	}
	
	@Override
	public void onImagePickCancelled() {
		normalToggle.doClick();
	}

}
