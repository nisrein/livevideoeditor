package com.ca.video.editor;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JPanel;
import org.opencv.core.Mat;
import org.opencv.core.Rect;

import com.ca.video.editor.VideoFrameEditor.MyEnumColor;

import common.ImageUtils;
import common.Log;
import common.MyColor;

public class MainVideoPanel extends JPanel implements WindowListener {
	private static final String TAG = MainVideoPanel.class.getSimpleName();
	private MyVideoCapture videoCap = new MyVideoCapture();
	private MyThread myThread;
	private boolean onVideoPreview = false;
	private boolean smartDetector = false;
	private Detector myFaceDetector;
	private Detector myFaceDetector2;
	private int frameCount;
	private int facesDetectedCount;
	private SupportedFeature currentFeature = SupportedFeature.IMAGE_NORMAL;
	private Mat selectedImage;
	private boolean drawDetectionBorder = true;

	/**
	 * 
	 */
	private static final long serialVersionUID = 8939119625643862780L;

	public MainVideoPanel() {
		init();
	}

	private void init() {
		File file = new File("resources/haarcascades/haarcascade_frontalface_alt2.xml");
		myFaceDetector = new Detector(file.getAbsolutePath());

		File file2 = new File("resources/haarcascades/haarcascade_frontalface_default.xml");
		myFaceDetector2 = new Detector(file2.getAbsolutePath());

		setVisible(true);
	}

	public void startVideoPreview() {
		Log.v(TAG, "startVideoPreview()");
		if (onVideoPreview || !this.isVisible())
			return;

		myThread = new MyThread();
		onVideoPreview = true;
		myThread.start();
	}

	public void stopVideoPreview() {
		Log.v(TAG, "stopVideoPreview()");
		if (onVideoPreview == false)
			return;
		onVideoPreview = false;
	}

	public boolean isSmartDetector() {
		return smartDetector;
	}

	public void setSmartDetector(boolean smartDetector) {
		this.smartDetector = smartDetector;
	}

	public Mat getSelectedImage() {
		return selectedImage;
	}

	public void setSelectedImage(Mat selectedImage) {
		this.selectedImage = selectedImage;
	}

	
	public boolean isDrawDetectionBorder() {
		return drawDetectionBorder;
	}

	public void setDrawDetectionBorder(boolean drawDetectionBorder) {
		this.drawDetectionBorder = drawDetectionBorder;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		setGraphicFont(g);

		frameCount++;
		if (onVideoPreview) {
			videoCap.capture();
		}
		Mat image = videoCap.getCapturedMat();

		Rect[] detections = detectMyFace(image);
		Rect[] detections2 = detectMyFace2(image);

		if (detections != null && detections.length > 0) {
			if (drawDetectionBorder) {
				VideoFrameEditor.drawRectangle(image, detections, MyEnumColor.MATERIAL_BLUE_500);
			}
			try {
				applyFeature(image, detections, currentFeature);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if ((detections != null && detections.length > 0) || (detections2 != null && detections2.length > 0)) {
			facesDetectedCount++;
		}

		ImageUtils.resize(image, 0.0f);
		g.drawImage(ImageUtils.bufferedImage(image), 0, 0, this);
		String str1 = "Frame Count: " + frameCount;
		String str2 = "Faces Count: " + facesDetectedCount;

		g.drawString(str1, 10, 20);
		g.drawString(str2, 10, 40);

	}

	private void applyFeature(Mat image, Rect[] detections, SupportedFeature supportedFeature) throws IOException {
		Log.v(TAG, "applyFeature: SupportedFeature: " + supportedFeature);
		switch (supportedFeature) {
		case IMAGE_BLUR:
			VideoFrameEditor.blur(image, detections);
			break;
		case IMAGE_PIXILATE:
			VideoFrameEditor.pixilate(image, detections);
			break;
		case IMAGE_CANNY:
			VideoFrameEditor.doCanny(image, detections, 10);
			break;
		case IMAGE_SOBEL:
			VideoFrameEditor.doSobel(image, detections, 6);
			break;
		case IMAGE_PICKER:
			VideoFrameEditor.doReplace(image, selectedImage, detections);
			break;
		case IMAGE_EROSION:
			VideoFrameEditor.erosion(image, detections, 7);
			break;
		case IMAGE_SWAP:
			int len = detections.length;
			if (len >= 2) {
				VideoFrameEditor.swap(image, detections[0], detections[1]);
			}
			break;
		case IMAGE_FLIP_DOWN:
			VideoFrameEditor.flipImageDown(image, detections);
			break;
		case IMAGE_FLIP_RIGHT:
				VideoFrameEditor.flipImageRight(image, detections);
				break;
		case IMAGE_BRIGHT:
			VideoFrameEditor.bright_Image(image, detections);
			break;
		case IMAGE_NORMAL:
		default:
			break;
		}

	}

	public void setCurrentFeature(SupportedFeature currentFeature) {
		this.currentFeature = currentFeature;
	}

	public SupportedFeature getCurrentFeature() {
		return currentFeature;
	}

	private void setGraphicFont(Graphics graphics) {
		Font font = new Font("TimesRoman", Font.BOLD, 20);
		graphics.setFont(font);
		graphics.setColor(MyColor.MATERIAL_DEEP_ORANGE_600);
	}

	class MyThread extends Thread {
		@Override
		public void run() {
			while (onVideoPreview) {
				repaint();
				try {
					Thread.sleep(30);
				} catch (InterruptedException e) {
				}
			}
		}
	}

	private Rect[] detectMyFace(Mat image) {
		if (!smartDetector) {
			return myFaceDetector.detect(image);
		} else {
			return myFaceDetector.smartDetect(image);
		}
	}

	private Rect[] detectMyFace2(Mat image) {
		return myFaceDetector2.detect(image);
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		this.stopVideoPreview();
		this.videoCap.release();

	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub

	}

}
