package com.ca.video.editor;

import java.awt.image.BufferedImage;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;

import common.ImageUtils;

public class MyVideoCapture {
	static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}

	private VideoCapture cap;
	private Mat capturedMat = new Mat();
	private float captureScale = 0f;

	public MyVideoCapture() {
		cap = new VideoCapture();
		cap.open(0);
	}

	public void capture() {
		cap.read(capturedMat);
		if(captureScale!=0) {
			ImageUtils.resize(capturedMat, captureScale);
		}
		
		Core.flip(capturedMat, capturedMat, 1);
	}
	
	
	public float getCaptureScale() {
		return captureScale;
	}

	public void setCaptureScale(float captureScale) {
		this.captureScale = captureScale;
	}

	public BufferedImage getBufferedImage() {
		return ImageUtils.bufferedImage(capturedMat);
	}
	
	public static BufferedImage getBufferedImage(Mat Img) {
		return ImageUtils.bufferedImage(Img);
	}

	public Mat getCapturedMat() {
		return capturedMat;
	}
	
	public void release() {
		cap.release();
	}
}
