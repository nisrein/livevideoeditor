package com.ca.video.editor;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import common.ImageUtils;

public class VideoFrameEditor {
	
	public static Mat drawRectangle(Mat image, Rect[] rects) {
		return drawRectangle(image, rects, MyEnumColor.MATERIAL_BLUE_500);
	}
	
	public static Mat drawRectangle(Mat image, Rect[] rects, MyEnumColor color) {
		for (Rect rect : rects) {
			drawRectangle(image, rect, color);
		}
		return image;
	}

	public static Mat drawRectangle(Mat image, Rect rect) {
		return drawRectangle(image, rect, MyEnumColor.MATERIAL_BLUE_500);
	}
	
	public static Mat drawRectangle(Mat image, Rect rect, MyEnumColor color) {
		Imgproc.rectangle(image, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height),
				new Scalar(color.getColorValue()), 4);
		return image;
	}

	public static Mat blur(Mat image, Rect[] rects) {
		for (Rect rect : rects) {
			blur(image, rect);
		}
		return image;
	}

	public static Mat blur(Mat image, Rect rect) {
		Size ksize = new Size(50, 50);
		Mat submat = image.submat(rect);
		Imgproc.blur(submat, submat, ksize);
		image.copyTo(submat);
		return image;
	}

	public static void pixilate(Mat image, Rect[] rects) {
		for (Rect rect : rects) {
			pixilate(image, rect);
		}
	}
	
	public static Mat pixilate(Mat image, Rect rect) {
		Mat submat = image.submat(rect);
		Mat result = pixilate(submat, 10);
		image.copyTo(result);
		return image;
		
	}
	
	public static Mat pixilate(Mat image, int pixSize) {
		Mat result = new Mat(image.width(), image.height(), image.type());
		
		for (int y = 0; y < image.height(); y += pixSize) {
			for (int x = 0; x < image.width(); x += pixSize) {

				// Copy the pixel
				double[] pixel = new double[3];
				pixel = image.get(x, y);

				// "Paste" the pixel onto the surrounding PIX_SIZE by PIX_SIZE neighbors
				// Also make sure that our loop never goes outside the bounds of the image
				for (int yd = y; (yd < y + pixSize) && (yd < result.height()); yd++) {
					for (int xd = x; (xd < x + pixSize) && (xd < result.width()); xd++) {
						image.put(xd, yd, pixel);
					}
				}
			}
		}
		
		return image;
	}
	
	public static BufferedImage pixilate(BufferedImage bufferedImage, int pixSize) {
		// Loop through every PIX_SIZE pixels, in both x and y directions
		Raster src = bufferedImage.getData();
		WritableRaster dest = src.createCompatibleWritableRaster();
		for (int y = 0; y < src.getHeight(); y += pixSize) {
			for (int x = 0; x < src.getWidth(); x += pixSize) {

				// Copy the pixel
				double[] pixel = new double[3];
				pixel = src.getPixel(x, y, pixel);

				// "Paste" the pixel onto the surrounding PIX_SIZE by PIX_SIZE neighbors
				// Also make sure that our loop never goes outside the bounds of the image
				for (int yd = y; (yd < y + pixSize) && (yd < dest.getHeight()); yd++) {
					for (int xd = x; (xd < x + pixSize) && (xd < dest.getWidth()); xd++) {
						dest.setPixel(xd, yd, pixel);
					}
				}
			}
		}
		
		BufferedImage bufferedImageResult = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), bufferedImage.getType());
		bufferedImageResult.setData(dest);
		return bufferedImageResult;
	}
	
	public static void doCanny(Mat image, Rect[] rects, int threshold) {
		for (Rect rect : rects) {
			image = doCanny(image, rect, threshold);
		}
	}
	
	public static Mat doCanny(Mat image, Rect rect, int threshold) {
		Mat src = new Mat(image, rect);
		
		Mat grayImage = new Mat();
		Mat detectedEdges = new Mat();
		
		Imgproc.cvtColor(src, grayImage, Imgproc.COLOR_BGR2GRAY);
		Imgproc.blur(grayImage, detectedEdges, new Size(3, 3));		
		Imgproc.Canny(detectedEdges, detectedEdges, threshold, threshold * 3, 3, false);		

		Mat dest = new Mat();
		src.copyTo(dest, detectedEdges);
		dest.copyTo(image.rowRange(rect.y, rect.height + rect.y).colRange(rect.x, rect.width + rect.x));

		return image;

	}
	
	public static void doSobel(Mat image, Rect[] rects, int kernelSize) {
		for (Rect rect : rects) {
			image = doSobel(image, rect, kernelSize);
		}
	}
	
	public static Mat doSobel(Mat image, Rect rect, int kernelSize) {
		Mat src = new Mat(image, rect);
		Mat grayImage = new Mat();
		Imgproc.cvtColor(src, grayImage, Imgproc.COLOR_BGR2GRAY);
		Mat destination = new Mat(src.rows(), src.cols(), src.type());
		
		Mat kernel = new Mat(kernelSize,kernelSize, CvType.CV_32F){
            {
               put(0,0,-1);
               put(0,1,0);
               put(0,2,1);

              /* put(1,0-2);
               put(1,1,0);
               put(1,2,2);*/

               put(2,0,-1);
               put(2,1,0);
               put(2,2,1);
            }
         };	
         
         Imgproc.filter2D(grayImage, destination, -1, kernel);
         Imgproc.cvtColor(destination, src, Imgproc.COLOR_GRAY2BGR);

 		src.copyTo(image.rowRange(rect.y, rect.height + rect.y).colRange(rect.x, rect.width + rect.x));
         
		return image;
	}
	
	public static void doReplace(Mat dest, Mat src, Rect[] rects) {
		for (Rect rect : rects) {
			doReplace(dest, src, rect);
		}
	}

	public static void doReplace(Mat dest, Mat src, Rect rect) {
		if (src != null && src.width() > 0 && src.height() > 0) {
			Mat tmp = new Mat(src.width(), src.height(), src.type());
			src.copyTo(tmp);
			Imgproc.resize(tmp, tmp, new Size(rect.width, rect.height));
			dest = ImageUtils.copyToROI(dest, tmp, rect);
		}
	}
	
	public static void erosion(Mat image,Rect[] rects, int erosion_size) {
		for (Rect rect : rects) {
			erosion(image, rect, erosion_size);
		}
	}
	
	public static void erosion(Mat image,Rect rect, int erosion_size) {
        Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new  Size(2*erosion_size + 1, 2*erosion_size+1));
        Mat submat = new Mat(image, rect);
		Imgproc.erode(submat, submat, element);
		
		image.copyTo(submat);
	}
	
	public static void swap(Mat image, Rect rect1, Rect rect2) {
		Size size1 = rect1.size();
		Size size2 = rect2.size();
		
		Mat submat1 = image.submat(rect1);
		Mat submat2 = image.submat(rect2);
		
		Mat temp1 = new Mat(submat1.width(), submat1.height(), submat1.type());
		Mat temp2 = new Mat(submat2.width(), submat2.height(), submat2.type());
		
		Imgproc.resize(submat2, temp1, size1);
		
		Imgproc.resize(submat1, temp2, size2);
		
		temp1.copyTo(submat1);
		temp2.copyTo(submat2);
		
		image.copyTo(submat1);
		image.copyTo(submat2);
	}
	
		
	public static void flipImageDown(Mat image,Rect[] rects) {
		for (Rect rect : rects) {
			flipImageDown(image, rect);
		}
	}
	
	public static void flipImageDown(Mat image,Rect rect) {
		Mat submat = new Mat(image, rect);
		Core.flip(submat, submat, 0);

		
		image.copyTo(submat);
	}
	
	public static void flipImageRight(Mat image,Rect[] rects) {
		for (Rect rect : rects) {
			flipImageRight(image, rect);
		}
	}
	
	public static void flipImageRight(Mat image,Rect rect) {
		Mat submat = new Mat(image, rect);
		Core.flip(submat, submat, 1);

		
		image.copyTo(submat);
	}
	
	public static void bright_Image(Mat image,Rect[] rects) {
		for (Rect rect : rects) {
			bright_Image(image, rect);
		}
	}
	public static void bright_Image(Mat image,Rect rect) {
		double alpha = 4;
		double beta = 50;
	
		Mat submat = new Mat(image, rect);
        submat.convertTo(submat, -1, alpha, beta);
 
        image.copyTo(submat);
	}
	
	public static enum MyEnumColor {
		MATERIAL_BLUE_500(0X21, 0X96, 0XF3, 0X0), 
		MATERIAL_ORANGE_900(0XE6, 0X51, 0X00, 0X0),
		MATERIAL_PINK_500(0XE9, 0X1E, 0X63, 0X00);
		private final double[] values;

		/**
		 * RGBA (RED, BLUE, GREEN, ALPHA)
		 * 
		 * @param red
		 * @param green
		 * @param blue
		 * @param alpha
		 */
		MyEnumColor(int red, int green, int blue, int alpha) {
			values = new double[4];
			values[0] = blue;
			values[1] = green;
			values[2] = red;
			values[3] = alpha;
		}

		public double[] getColorValue() {
			return values;
		}
	}
}

