package common;

import java.awt.Component;
import java.io.File;
import javax.swing.JFileChooser;

public class ImagePicker {
	public static final String TAG = ImagePicker.class.getSimpleName();
	private static final String newline = "\n";
	private StringBuilder log;
	private JFileChooser fc;
	private OnImagePickerListener mOnImagePickerListener;

	public ImagePicker() {
		log = new StringBuilder();
	}

	public void show(Component parent) {
		// Set up the file chooser.
		if (fc == null) {
			fc = new JFileChooser();

			// Add a custom file filter and disable the default
			// (Accept All) file filter.
			fc.addChoosableFileFilter(new ImageFilter());
			fc.setAcceptAllFileFilterUsed(false);

			// Add custom icons for file types.
			fc.setFileView(new ImageFileView());

			// Add the preview pane.
			fc.setAccessory(new ImagePreview(fc));
		}

		// Show it.
		int returnVal = fc.showDialog(parent, "Attach");

		// Process the results.
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			informListenerOnImagePicked(file.getAbsolutePath());
			log.append("Attaching file: " + file.getName() + newline);
		} else {
			log.append("Attachment cancelled by user." + newline);
			informListenerOnImagePickCancelled();
		}
		Log.v(TAG, log.toString());
		// Reset the file chooser for the next time it's shown.
		fc.setSelectedFile(null);
	}
	
	private void informListenerOnImagePicked(String filePath) {
		if(mOnImagePickerListener!=null) {
			mOnImagePickerListener.onImagePicked(filePath);
		}
	}
	
	private void informListenerOnImagePickCancelled() {
		if(mOnImagePickerListener!=null) {
			mOnImagePickerListener.onImagePickCancelled();
		}
	}
	
	public void setOnImagePickerListener(OnImagePickerListener listener) {
		this.mOnImagePickerListener = listener;
	}
	
	public interface OnImagePickerListener {
		void onImagePicked(String filePath);
		void onImagePickCancelled();
	}
}
