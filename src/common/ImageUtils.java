package common;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class ImageUtils {
	
	public static void resize(Mat image, float scale) {
		if(scale==0) return;
			int w = image.width()  + (int) (image.width() *  scale);
			int h = image.height()  + (int) (image.height() *  scale);
			Size sz = new Size(w,h);
			Imgproc.resize( image, image,sz);
	}
	
	public static BufferedImage bufferedImage(Mat image) {
		int type = BufferedImage.TYPE_BYTE_GRAY;
		if (image.channels() > 1) {
			type = BufferedImage.TYPE_3BYTE_BGR;
		}
		BufferedImage bufferedImage = new BufferedImage(image.cols(), image.rows(), type);

		image.get(0, 0, ((DataBufferByte) bufferedImage.getRaster().getDataBuffer()).getData());
		return bufferedImage;
	}
	
	public static Mat matImage(BufferedImage bufferedImage) {
		byte[] pixels = ((DataBufferByte) bufferedImage.getRaster().getDataBuffer()).getData();
		Mat image = new Mat(bufferedImage.getWidth(), bufferedImage.getHeight(), CvType.CV_8UC3);
		image.put(0, 0, pixels);
		
		return image;
	}
	
	public static Mat rotate(Mat image, double angle) {
		Point center = new Point(image.width() / 2, image.height() / 2);
		Mat rotImage = Imgproc.getRotationMatrix2D(center, angle, 1.0);
		
		 Mat dst = new Mat();
		 Size size = new Size(image.width(), image.height());
		 
		 Imgproc.warpAffine(image, dst, rotImage, size);
		
		return dst;
	}
	
	public static Mat copyToROI(Mat dest, Mat src, Rect rect) {
		src.copyTo(dest.rowRange(rect.y, rect.height + rect.y).colRange(rect.x, rect.width + rect.x));
		return dest;
	}
	
	public static Mat readImage(String filePath) {
		return Imgcodecs.imread(filePath);
	}
}
