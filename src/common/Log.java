package common;

import com.ca.video.editor.MainApplication;

public class Log {
	private static final String TAG = MainApplication.class.getSimpleName();
	
	public static void v(String msg) {
		v(TAG, msg);
	}
	
	public static void v(String tag, String msg) {
		System.out.println(tag + ": " + msg);
	}
	
	public static void e(String msg) {
		v(TAG, msg);
	}
	
	public static void e(String tag, String msg) {
		System.out.println(tag + ": " + msg);
	}
}
