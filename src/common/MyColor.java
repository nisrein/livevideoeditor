package common;

import java.awt.Color;
import java.awt.color.ColorSpace;

public class MyColor extends Color {
	private static final long serialVersionUID = 4381789692415166043L;
	
	public static Color MATERIAL_BLUE_500 = new Color(0X21, 0X96, 0XF3);
	public static Color MATERIAL_ORANGE_900 = new Color(0XE6, 0X51, 0X00);
	public static Color MATERIAL_DEEP_ORANGE_600 = new Color(0XF4, 0X51, 0X1E);
	
	public MyColor(ColorSpace cspace, float[] components, float alpha) {
		super(cspace, components, alpha);
		// TODO Auto-generated constructor stub
	}

	public MyColor(float r, float g, float b, float a) {
		super(r, g, b, a);
		// TODO Auto-generated constructor stub
	}

	public MyColor(float r, float g, float b) {
		super(r, g, b);
		// TODO Auto-generated constructor stub
	}

	public MyColor(int rgba, boolean hasalpha) {
		super(rgba, hasalpha);
		// TODO Auto-generated constructor stub
	}

	public MyColor(int r, int g, int b, int a) {
		super(r, g, b, a);
		// TODO Auto-generated constructor stub
	}

	public MyColor(int r, int g, int b) {
		super(r, g, b);
		// TODO Auto-generated constructor stub
	}

	public MyColor(int rgb) {
		super(rgb);
		// TODO Auto-generated constructor stub
	}
	
	
}
